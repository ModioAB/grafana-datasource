export interface VisEditorOptions {
  jsonData: string;
  dataSourceUid: string;
  trendGrafanaId: string;
}
