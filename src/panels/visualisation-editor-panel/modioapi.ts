/**
 * This file is inspired by example form visualisation-editor-project.
 * Functions should probably be refactored into ModioApi DatasSource instead of injected
 * into create.
 * The timers has been removed and relays on grafana dashbaord reload
 * (which triggers re-render of component and new fetch).
 */

import { FetchResponse } from '@grafana/runtime';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

function create({ modioApi, currentUser = 'anonymous' }) {
  let hostsObservable = null as any;
  let hostObservables = new Map();
  let sensorObservables = new Map();
  let self;
  let baseURI = `${modioApi.url}/v0`;
  let _fetch = (opts): Observable<FetchResponse<unknown>> => modioApi.backendSrv.fetch(opts);
  let _post = (opts, data): Observable<FetchResponse<unknown>> => modioApi.backendSrv.post(opts, data);

  function getHosts() {
    if (hostsObservable === null) {
      hostsObservable = _fetch({
        url: `${baseURI}/hosts`,
      }).pipe(
        map((response) => {
          return new Map((response as any).data.map((host) => [host.host, { name: host.name }]));
        })
      );
    }
    return hostsObservable;
  }

  function getSensorsForHost(host) {
    if (!hostObservables.has(host)) {
      hostObservables.set(
        host,
        _fetch({
          url: `${baseURI}/${host}`,
        }).pipe(
          map((response) => {
            return new Map(
              (response as any).data.map((sensor) => {
                const { key, lastvalue, lastclock, name: desc, unit } = sensor;
                return [key, { lastvalue, lastclock, desc, unit }];
              })
            );
          })
        )
      );
    }
    return hostObservables.get(host);
  }

  function getSensorValue(host, key) {
    const hostAndKey = `${host}:${key}`;
    if (!sensorObservables.has(hostAndKey)) {
      const url = `${baseURI}/${host}/${key}`;
      sensorObservables.set(
        hostAndKey,
        _fetch({ url }).pipe(
          map((response) => {
            let { key, lastvalue, lastclock, configurable, config, name, unit } = response.data as any;
            if (typeof lastclock === 'string') {
              try {
                lastclock = Date.parse(lastclock).valueOf() / 1000;
              } catch (error) {
                console.error(error);
                lastclock = null;
              }
            }
            const transformed = { key, lastvalue, lastclock, configurable, desc: name, config, i18nUnit: unit };
            if (configurable) {
              const makeConfig = (targetText, cb) => {
                const expected = lastvalue;
                let target = targetText;
                if (typeof expected === 'number') {
                  target = parseFloat(targetText);
                }
                const audit = [currentUser];
                const postData = { target, audit, expected };
                const postObservable = _post(`${url}/config`, postData);
                postObservable.subscribe(cb);
              };
              // @ts-ignore
              transformed.makeConfig = makeConfig;
            }
            return transformed;
          })
        )
      );
    }
    return sensorObservables.get(hostAndKey);
  }

  self = {
    getHosts,
    getSensorsForHost,
    getSensorValue,
  };

  return self;
}

export const ModioAPI = { create };
