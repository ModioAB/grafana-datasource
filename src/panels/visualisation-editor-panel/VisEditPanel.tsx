import React from 'react';
import ReactDOM from 'react-dom';
import { of } from 'rxjs';
import { PanelProps } from '@grafana/data';
import { config, getDataSourceSrv } from '@grafana/runtime';

import { VisEditorOptions } from './types';
import { Editor } from '../../../static/editor.bundle.js';
import { Visualisation, GEROX_WIDGETS, BASPIC_WIDGETS, MODIO_WIDGETS } from '../../../static/visualisation.bundle.js';
import { ModioAPI } from './modioapi';
import '../../../static/style.css';
import './grafana-style.scss';
import { ModioApiDataSource } from 'datasources/modio-api-datasource/DataSource';
import { GraphModal } from './GraphModal';
import { Alert } from '@grafana/ui';

interface Props extends PanelProps<VisEditorOptions> {}
interface State {}

export const MAGIC_PLACEHOLDER_TEXT = 'Magic Placeholder';
export class VidEditPanel extends React.Component<Props, State> {
  private _sensorAPI: any;
  visualElement!: HTMLDivElement | null;
  textareaElement!: HTMLTextAreaElement | null;
  isEditMode: Boolean = false;
  editor!: any;
  json: any;

  constructor(props: Props) {
    super(props);
    const currentUser = config.bootData.user.name; // name or ID for audit log?
    const { options } = props;
    const { dataSourceUid } = options;
    const dsSrv = getDataSourceSrv();
    const dataSourceSettings = dsSrv.getInstanceSettings(dataSourceUid);
    if (dataSourceSettings) {
      const modioApi: ModioApiDataSource = new ModioApiDataSource(dataSourceSettings);
      this._sensorAPI = ModioAPI.create({ modioApi, currentUser });
      // modioApi.backendSrv.fetch({ url: '' }).pipe(map((resp) => resp.data));
    }
  }

  render() {
    this.textareaElement = document.querySelector(
      `textarea[placeholder="${MAGIC_PLACEHOLDER_TEXT}"]`
    ) as HTMLTextAreaElement;
    this.isEditMode = this.textareaElement !== null;
    if (!this._sensorAPI) {
      console.error('We have no datasource for sensors');
      return <div>Problem</div>;
    }
    return <div ref={(e) => (this.visualElement = e)} />;
  }

  componentDidMount() {
    this.setupVisualisation();
  }

  componentDidUpdate(prevProps, prevState) {
    this.setupVisualisation();
  }

  saveEditor() {
    if (this.editor && this.editor.haveUnsavedChanges()) {
      this.json = this.editor.save();
      // HACK: this 'saves' jsonData to side-panel
      // and makes it the textfield dirty, so it is saved on panel-save
      const textarea = this.textareaElement;
      if (textarea) {
        textarea.focus();
        textarea.select();
        textarea.value = JSON.stringify(this.json);
        textarea.blur();
      } else {
        console.warn('Save error', 'Textarea with jsonData not found', MAGIC_PLACEHOLDER_TEXT);
      }
    }
  }

  setupVisualisation() {
    const {
      options: { jsonData },
      width,
      height,
    } = this.props;

    const trendGrafanaElem = document.getElementById(this.props.options.trendGrafanaId);
    if (trendGrafanaElem) {
      trendGrafanaElem.style.top = '0px';
      trendGrafanaElem.style.left = '0px';
      trendGrafanaElem.style.width = '100%';
      trendGrafanaElem.classList.add('nativeTrend');
    }

    this.json = JSON.parse(jsonData);
    this.json.maxHeight = height;
    this.json.maxWidth = width;
    const visualCreator = this.isEditMode ? Editor : Visualisation;
    this.editor = visualCreator
      .create({
        element: this.visualElement,
        sensorAPI: this._sensorAPI, // we warn if missing in render function
        backgroundStore: this,
      })
      .registerWidgetTypes(GEROX_WIDGETS)
      .registerWidgetTypes(BASPIC_WIDGETS)
      .registerWidgetTypes(MODIO_WIDGETS);
    if (this.isEditMode) {
      this.editor.addAfterEditCallback(() => this.saveEditor());
    }
    // cannot chain this to `create` above due to
    // difference between `Editor.load()` and `VisualisationEditor.load()`

    this.editor.load(this.json);

    if (!this.isEditMode) {
      // Trend data only when not in edit mode
      this.checkTrendPanel();
      this.linkTrendData();
    }
  }

  checkTrendPanel() {
    // Checking wether we have a panel for trend graphs
    const existsAlertElem = document.getElementById('alertElem');

    if (this.props.options.trendGrafanaId) {
      const trendPanel = document.getElementById(this.props.options.trendGrafanaId);
      if (trendPanel) {
        if (existsAlertElem) {
          existsAlertElem.style.display = 'none';
        }
        return;
      }
    }
    const dashboardWrapper = Array.from(document.getElementsByClassName('scrollbar-view') as HTMLCollectionOf<HTMLElement>);
    const alertElem = document.createElement('div');
    alertElem.id = 'alertElem';
    if (existsAlertElem) {
      existsAlertElem.style.display = 'flex';
    } else {
      alertElem.style.display = 'flex';
      if (dashboardWrapper.length > 0) {
        const dashboardDiv = dashboardWrapper[0];
        if (dashboardDiv) {
          dashboardDiv.appendChild(alertElem); 
        }
      }
    }

    ReactDOM.render(
      <Alert
        title='Problem with id for trend graphs'
        severity='warning'
      >
        <p>
          There seems to be a problem with the panel for trend graphs.
          Please refer to the documentation in order to set things up properly.
        </p>
      </Alert>,
      alertElem
    );
  };
  linkTrendData = (): void => {
    this.editor.setPopupCallback((data) => {
      const {
        options: { jsonData },
      } = this.props;
      this.json = JSON.parse(jsonData);
      let key = '';
      try {
        if (data.widget.sensor) {
          key = data.widget.sensor.key;
        } else {
          key = data.widget.sensorValue.key;
        }
      } catch (error) {
        console.log(error);
        console.log(data);
      }
      const trendModalElem = document.createElement('div');
      trendModalElem.id = 'trendModal';
      data.element.appendChild(trendModalElem);
      ReactDOM.render(
        <GraphModal
          modioKey={key}
          trendGrafanaId={this.props.options.trendGrafanaId}
        />,
        trendModalElem
      );
      return;
    });
  };

  getBackgrounds() {
    console.log('TODO', 'getBackgrounds not implemented yet');
  }
  getBackgroundNode(svg) {
    const parser = new DOMParser();
    const node = parser.parseFromString(svg, 'image/svg+xml').documentElement;
    return of(node);
  }
  getBackgroundLayers(svg) {
    const parser = new DOMParser();
    const domNode = parser.parseFromString(svg, 'image/svg+xml').documentElement;
    const layerNodes = domNode.querySelectorAll('g[id]');
    const layers = [] as any[];
    const layerNames = new Set();
    layerNodes.forEach((node) => {
      const name = node.id;
      if (!layerNames.has(name)) {
        layerNames.add(name);
        const visible = node.getAttribute('display') !== 'none';
        layers.push({ name, visible });
      } else {
        console.warn(`Layer name ${name} is duplicated`);
      }
    });
    layers.sort((a, b) => (a.name < b.name ? -1 : 0));
    return of(layers);
  }
}
