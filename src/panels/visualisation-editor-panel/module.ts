import { PanelPlugin } from '@grafana/data';
import { getDataSourceSrv } from '@grafana/runtime';
import { VisEditorOptions } from './types';
import { VidEditPanel, MAGIC_PLACEHOLDER_TEXT } from './VisEditPanel';

export const plugin = new PanelPlugin<VisEditorOptions>(VidEditPanel).setPanelOptions((builder) => {
  return builder
    .addSelect({
      path: 'dataSourceUid',
      name: 'Data source',
      settings: {
        options: getDataSourceSrv()
          .getList({ pluginId: 'modioab-api-datasource' })
          .map((ds) => {
            return { label: ds.name, value: ds.uid };
          }),
      },
    })
    .addTextInput({
      path: 'jsonData',
      description: 'Json data to define the visualisation',
      defaultValue: '{}',
      name: 'Json Data',
      settings: {
        placeholder: MAGIC_PLACEHOLDER_TEXT,
        useTextarea: true,
        rows: 5,
      },
    })
    .addTextInput({
      path: 'trendGrafanaId',
      description: 'ID for trend graph',
      defaultValue: 'panel-4',
      name: 'Trend Graph ID'
    })
});
