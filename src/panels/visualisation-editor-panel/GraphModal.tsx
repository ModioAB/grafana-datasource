import React from 'react';
import { getLocationSrv } from '@grafana/runtime';
import { Icon } from '@grafana/ui';

import '../../../static/style.css';
import './grafana-style.scss';

interface TrendPanelProps {
  modioKey: string;
  trendGrafanaId: string;
}

export class GraphModal extends React.Component<TrendPanelProps> {
  constructor(props: TrendPanelProps) {
    super(props);
  }
  state = {
    openTrendModal: false,
  };
  onClickButton = (e: any) => {
    e.preventDefault();
    this.setState({ openTrendModal: true });
    this.setTrendItem();
  };
  onCloseModal = () => {
    this.closeModal();
    this.setState({ openTrendModal: false });
  };
  render() {
    return (
      <div id="modal">
        <div id="trendModalButton">
          <button onClick={this.onClickButton}>
            <span>
              <Icon name="chart-line" /> View trend
            </span>
          </button>
        </div>
      </div>
    );
  }
 
  setTrendItem() {
    // According to https://grafana.com/docs/grafana/latest/developers/plugins/add-support-for-variables/#set-a-variable-from-your-plugin
    // the correct way to set the variable is using getLocationSrv().update instead of templateSrv().replace
    getLocationSrv().update({
      query: {
        'var-item': this.props.modioKey
      },
      partial: true,
      replace: true,
    });
    const trendPanel = document.getElementById(this.props.trendGrafanaId);
    if (trendPanel) {
      trendPanel.style.visibility = 'visible';
      trendPanel.style.opacity = '1';
      trendPanel.style.height = '100vh';
      trendPanel.style.setProperty('position', 'fixed', 'important');
      const existsCloseButton = document.getElementById('closeButton');
      if (!existsCloseButton) {
        const closeButton = document.createElement('a');
        closeButton.appendChild(document.createTextNode('X'));
        closeButton.href = '#';
        closeButton.id = 'closeButton';
        closeButton.onclick = this.onCloseModal;
        trendPanel.appendChild(closeButton);
      }
    }
    return;
  }

  closeModal() {
    const trendPanel = document.getElementById(this.props.trendGrafanaId);
    if (trendPanel) {
      trendPanel.style.visibility = 'hidden';
      trendPanel.style.opacity = '0';
    }
  }

}