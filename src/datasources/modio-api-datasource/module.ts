import { DataSourcePlugin } from '@grafana/data';
import { ModioApiDataSource } from './DataSource';
import { ConfigEditor } from './ConfigEditor';
import { QueryEditor } from './QueryEditor';
import { ModioApiQuery } from './types';

export const plugin = new DataSourcePlugin<ModioApiDataSource, ModioApiQuery>(ModioApiDataSource)
  .setConfigEditor(ConfigEditor)
  .setQueryEditor(QueryEditor);
