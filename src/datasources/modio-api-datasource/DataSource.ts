import {
  DataQueryRequest,
  DataQueryResponse,
  DataSourceApi,
  DataSourceInstanceSettings,
  FieldType,
  MetricFindValue,
  MutableDataFrame,
  MappingType,
  ThresholdsMode,
  dateTimeFormatTimeAgo,
  stringToMs,
  FieldConfig,
  DataSourceJsonData,
  TimeSeriesPoints,
  TimeSeriesValue,
} from '@grafana/data';
import { BackendSrv, BackendSrvRequest, getBackendSrv, getTemplateSrv, TemplateSrv } from '@grafana/runtime';
import * as d3 from 'd3';
import { grafanaUnit } from './grafanaUnit';

import { ModioApiQuery, ApiPrototype } from './types';
const TREND_BREAKPOINT = stringToMs('30m');
const HOUR = stringToMs('1h');

interface TimeMatrix {
  [key: number]: TimeSeriesPoints;
}
interface ItemQueryData {
  label: string;
  value: string;
  description: string;
  lastvalue: any;
  lastclock: any;
  unit: string;
  host: string;
  api_key: string;
  prototypeName: string;
}
export class ModioApiDataSource extends DataSourceApi<ModioApiQuery, DataSourceJsonData> {
  url: string | undefined;
  private ongoingRequests: Map<string, any>;
  backendSrv: BackendSrv;
  templateSrv: TemplateSrv;

  private readonly adhocFilterComparators = {
    '=': (a, b) => a === b,
    '!=': (a, b) => a !== b,
    '<': (a, b) => a < b,
    '>': (a, b) => a > b,
    '=~': (a, b) => a.match(b),
    '!~': (a, b) => !a.match(b),
  };

  constructor(instanceSettings: DataSourceInstanceSettings<DataSourceJsonData>) {
    super(instanceSettings);
    this.url = instanceSettings.url;
    this.backendSrv = getBackendSrv();
    this.templateSrv = getTemplateSrv();
    this.ongoingRequests = new Map();
    this.interval = '90s'; // Default for Query options "Min interval"
  }

  async testDatasource() {
    try {
      await this.backendSrv.fetch({ url: `${this.url}/v0/ping` }).toPromise();
      return { status: 'success', message: 'Data source is working' };
    } catch (error) {
      let message: string;
      if (typeof error.data === 'object' && error.data.title) {
        message = `Error: ${error.data.status} ${error.data.title} - ${error.data.detail}`;
      } else if (typeof error.err === 'object') {
        if (error.err.status === -1) {
          message = `Error: XHR failed - Check the developer console for details.`;
        } else {
          message = `Error: ${error.err.status} ${error.err.statusText}`;
        }
      } else {
        if (error.status === 502) {
          message = `Error: ${error.status} ${error.statusText} - Check the Grafana server logs for details.`;
        } else {
          message = `Error: ${error.status} ${error.statusText}`;
        }
      }
      return { message, status: 'error' };
    }
  }
  async query(options: DataQueryRequest<ModioApiQuery>): Promise<DataQueryResponse> {
    const {
      range: { from, to },
      intervalMs,
      scopedVars,
      targets,
    } = options;

    const fromTime = Math.floor(from.valueOf() / 1000);
    const toTime = Math.ceil(to.valueOf() / 1000);
    const useTrends = intervalMs > TREND_BREAKPOINT;
    const adjustedIntervalMs = useTrends ? Math.max(HOUR, intervalMs) : intervalMs;
    const timeInterval = d3.timeMillisecond.every(adjustedIntervalMs)!;
    const frames = targets.map(async (target) => {
      const { refId, hide, prototypeId, key, aggregation = 'avg' } = target;
      const frame = new MutableDataFrame({
        refId,
        fields: [{ name: 'time', type: FieldType.time, config: { displayName: 'Time', custom: { width: 147 } } }],
      });
      if (hide) {
        return frame;
      }
      const scopedPrototypeId = this.templateSrv.replace(prototypeId, scopedVars, 'regex');
      const scopedKey = this.templateSrv.replace(key, scopedVars, 'regex');
      if (key === 'trigger') {
        return this.addTriggersToFrame(frame, scopedPrototypeId);
      }

      const dataseries: Array<Promise<TimeSeriesPoints>> = [];
      const items = await this.itemQuery(scopedPrototypeId, scopedKey);

      for (const item of items) {
        const { host, value, prototypeName, api_key, label: displayNameFromDS } = item;
        const name = `${host}:${value}`;
        const [model, unitId, version] = value.split('.');
        const [unit, multiplier = 1] = grafanaUnit(item.unit, model, version);
        const displayName = this.templateSrv.replace(target.alias, scopedVars, 'regex') || undefined;
        const config: FieldConfig = {
          unit,
          displayName,
          displayNameFromDS,
          writeable: true,
          custom: {},
        };
        frame.addField({
          name,
          type: FieldType.number,
          config,
          labels: {
            model,
            unitId,
            version,
            host,
            prototypeName,
            api_key,
            name: displayNameFromDS,
            itemKey: value,
          },
        });
        let request: Promise<TimeSeriesPoints>;
        if (useTrends) {
          request = this.requestAvgTrend(host, value, fromTime, toTime, multiplier);
        } else {
          request = this.requestHistory(host, value, fromTime, toTime, multiplier);
        }
        dataseries.push(request);
      }
      const timeMatrix = new Object() as TimeMatrix;
      const dateRange = timeInterval.range(from.toDate(), to.toDate());
      for (const date of dateRange) {
        // cannot use .fill([]) since it copies [] by ref
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/fill#description
        timeMatrix[date.getTime()] = [...new Array(items.length)].map(() => []);
      }

      (await Promise.all(dataseries)).forEach((serie, idx) => {
        // populate values:
        for (const [time, value] of serie) {
          const ts = timeInterval.round(new Date(time!)).getTime();
          // needed to ensure the rounded time is within our interval
          const timeRow = timeMatrix[ts];
          if (timeRow) {
            timeRow[idx].push(value);
          }
        }
      });

      for (const date of dateRange) {
        const ts = date.getTime();
        const timeRow = timeMatrix[ts] as number[][];
        const aggregated: TimeSeriesValue[] = [];
        for (let vals of timeRow) {
          let avg: number | null = null;
          if (vals.length > 0) {
            avg = vals.reduce((prev, curr) => prev + curr, 0) / vals.length;
          }
          aggregated.push(avg);
        }
        frame.appendRow([ts, ...aggregated]);
      }
      if (aggregation === 'delta') {
        this.aggregateRowDeltas(frame);
      }
      return frame;
    });

    const data = await Promise.all(frames);
    return { data };
  }

  private aggregateRowDeltas(frame: MutableDataFrame<any>) {
    const [times, ...dataFields] = frame.fields;
    for (const dataField of dataFields) {
      let timeFactor = 1;
      const {
        values,
        config: { unit },
      } = dataField;
      if (unit === 'watth') {
        dataField.config.unit = 'watt';
        timeFactor *= stringToMs('1h');
      }
      let prevVal;
      let prevTime;
      for (let index = 0; index < values.length; index++) {
        const val = values.get(index);
        const ts = times.values.get(index);
        if (prevVal !== undefined && prevTime !== null && val !== null) {
          values.set(index, (timeFactor * (val - prevVal)) / (ts - prevTime));
        } else {
          values.set(index, null);
        }
        if (val !== null) {
          prevVal = val;
          prevTime = ts;
        }
      }
    }
  }

  async addTriggersToFrame(frame: MutableDataFrame<any>, scopedPrototypeId: string): Promise<MutableDataFrame<any>> {
    this.addTriggerFieldsToFrame(frame);

    // @ts-expect-error
    const filters = this.templateSrv.getAdhocFilters(this.name);
    const prototypes = await this.prototypeQuery(scopedPrototypeId);
    const triggerRequests = prototypes.map(async ({ host, site, name: prototypeName }) => {
      const triggers = await this.doRequest({ url: `/v0/${host}/trigger` });
      return { site, prototypeName, triggers };
    });
    const prototypeTriggers = await Promise.all(triggerRequests);

    for await (const { site, prototypeName, triggers } of prototypeTriggers) {
      for (const trigger of triggers) {
        const { triggerid: id, severity, lastchange, description, value } = trigger;
        const time = lastchange * 1000;
        const timeAgo = dateTimeFormatTimeAgo(time);
        const active = value === 'problem';
        const [, severityName] = severity;
        const frameValues = {
          time,
          timeAgo,
          Problem: active,
          id,
          description,
          severityName,
          // severityId,
          site,
          prototypeName,
        };
        let skipDataRow = false;
        for (const { key, operator, value } of filters) {
          if (!this.adhocFilterComparators[operator](frameValues[key], value)) {
            skipDataRow = true;
          }
        }
        if (!skipDataRow) {
          frame.add(frameValues);
        }
      }
    }
    return frame;
  }
  /**
   * Mutates frame to have fields suitable for displaying problem panel
   * @param frame
   */
  private addTriggerFieldsToFrame(frame: MutableDataFrame<any>) {
    frame.addField({
      name: 'timeAgo',
      type: FieldType.string,
      config: { displayName: 'Last changed', custom: { width: 137 } },
    });
    frame.addField({
      name: 'Problem',
      type: FieldType.boolean,
      config: {
        custom: { displayMode: 'color-background', width: 100, align: 'center' },
        filterable: true,
        thresholds: {
          mode: ThresholdsMode.Absolute,
          steps: [
            { value: -Infinity, color: 'green' },
            { value: 1, color: 'red' },
          ],
        },
        mappings: [
          {
            id: 0,
            type: MappingType.ValueToText,
            // @ts-expect-error
            value: false,
            text: 'OK',
          },
          {
            id: 1,
            type: MappingType.ValueToText,
            // @ts-expect-error
            value: true,
            text: 'PROBLEM',
          },
        ],
      },
    });
    frame.addField({ name: 'description', type: FieldType.string, config: { custom: { width: 350 } } });
    frame.addField({
      name: 'severityName',
      type: FieldType.string,
      config: { filterable: true, displayNameFromDS: 'Severity', custom: { width: 65 } },
    });
    frame.addField({ name: 'site', type: FieldType.string });
    frame.addField({ name: 'prototypeName', type: FieldType.string, config: { displayNameFromDS: 'Prototype' } });
    frame.addField({ name: 'id', type: FieldType.string, config: { unit: 'none' } });
  }

  /**
   * Simple query language.
   * @param query query is colon-separated in format site:device:item
   * @param options filled
   */
  async metricFindQuery(query: string, options?: any) {
    const [siteQuery, deviceQuery, itemQuery] = query.split(':');
    let result = new Set<MetricFindValue>();
    const siteProtos = await this.siteQuery(siteQuery);
    const sites = new Set<MetricFindValue>(siteProtos.map((s) => ({ text: s.site, value: s.api_key })));

    if (!deviceQuery) {
      result = sites;
    } else {
      const prototypes = await this.prototypeQuery(deviceQuery);
      for (const device of prototypes) {
        if ([...sites.values()].find((s) => s.value === device.api_key)) {
          // device query
          if (!itemQuery) {
            result.add({ text: device.label, value: device.id });
          } else {
            // item query
            const items = await this.itemQuery(device.id, itemQuery);
            for (const item of items) {
              result.add({ text: item.label });
            }
          }
        }
      }
    }

    return [...result];
  }

  async getPrototype(prototypeId: string) {
    const proto = await this.doRequest({ url: `/v0/prototypes/${prototypeId}` });
    // the 404 return an array, but found prototype returns object.
    if (Array.isArray(proto)) {
      return null;
    }
    return this.transformDevice(proto);
  }

  private transformDevice(prototype: ApiPrototype) {
    const { site, name, id, host } = prototype;
    return {
      ...prototype,
      label: `${site} / ${name}`,
      value: id,
      description: host,
    };
  }

  async siteQuery(rawQuery: string) {
    const prototypes = await this.requestPrototypes();
    const query = this.templateSrv.replace(rawQuery, undefined, 'regex');
    const re = toRegexp(query);

    return prototypes.filter((p) => re.test(p.api_key) || re.test(p.site)); // TODO more???
  }

  async prototypeQuery(rawQuery?: string) {
    const response = await this.requestPrototypes();
    let data = response.map((proto) => this.transformDevice(proto));
    const query = this.templateSrv.replace(rawQuery, undefined, 'regex');
    const re = toRegexp(query!);
    data = data.filter(
      (proto) => proto.value === rawQuery || proto.value === query || re.test(proto.label) || proto.host === query
    );
    data.sort(compareLabel);
    return data;
  }

  async itemQuery(prototypeId?: string, rawQuery?: string) {
    const data: ItemQueryData[] = [];
    if (!prototypeId) {
      return data;
    }
    if (!rawQuery) {
      return data;
    }
    const query = this.templateSrv.replace(rawQuery, undefined, 'regex');
    if (!query) {
      return data;
    }
    const re = toRegexp(query);

    for (const { host, api_key, name: prototypeName } of await this.prototypeQuery(prototypeId)) {
      for (const item of await this.doRequest({ url: `/v0/${host}` })) {
        const { lastclock, lastvalue, name, key: value, unit } = item;
        if (re.test(name) || re.test(value) || re.test(unit)) {
          const itemQueryData: ItemQueryData = {
            label: lastclock === null ? `(${name})` : name,
            value,
            description: `${host}:${value}, ${lastvalue} ${unit}`,
            lastvalue,
            lastclock,
            unit,
            host,
            api_key,
            prototypeName,
          };
          data.push(itemQueryData);
        }
      }
    }
    data.sort(compareItems);
    return data;
  }

  private async newRequest(apiUrl: string, options: BackendSrvRequest, fallback) {
    try {
      const response = await this.backendSrv.fetch(options).toPromise();
      if (response.status === 200) {
        return response.data;
      }
    } catch (error) {
      if (error.status === 404) {
        if ('redirect_location' in error.data) {
          options.url = error.data.redirect_location;
          return await this.doRequest(options, fallback);
        }
      } else {
        // TODO Force logout when 401 (UNAUTHORIZED)?
        throw error;
      }
    } finally {
      this.ongoingRequests.delete(apiUrl);
    }
    return fallback;
  }

  private async doRequest(options, fallback = []) {
    const apiUrl = options.url;
    options.url = this.url + apiUrl;
    if (!this.ongoingRequests.has(apiUrl)) {
      const newPromise = this.newRequest(apiUrl, options, fallback);
      this.ongoingRequests.set(apiUrl, newPromise);
    }
    const existingPromise = this.ongoingRequests.get(apiUrl);
    return existingPromise;
  }

  private requestPrototypes(): Promise<ApiPrototype[]> {
    return this.doRequest({ url: '/v0/prototypes' });
  }

  private async requestHistory(host, key, fromTime, toTime, multiplier): Promise<TimeSeriesPoints> {
    const itemUrl = `/v0/${host}/${key}`;
    const url = `${itemUrl}/history_prepare/${fromTime}/${toTime}`;
    const preparedUrls: string[] = await this.doRequest({ url });
    // if toTime is less than 15 min ago?
    if (Date.now() / 1000 - toTime < 15 * 60) {
      preparedUrls.push(`${itemUrl}/history`);
    }
    const partRequests = preparedUrls.map(async (url) => {
      const data = await this.doRequest({ url });
      return data.map(([time, value]): TimeSeriesValue[] => [time * 1000, toNumber(value, multiplier)]);
    });
    const parts = await Promise.all(partRequests);
    return ([] as any).concat(...parts);
  }

  private async requestAvgTrend(host, key, fromTime, toTime, multiplier): Promise<TimeSeriesPoints> {
    // TODO: trend api have upper limit for interval: 1 year
    const url = `/v0/${host}/${key}/trend/avg/${fromTime}/${toTime}`;
    const data: TimeSeriesPoints = await this.doRequest({ url });
    return data.map(([time, value]): TimeSeriesValue[] => [time! * 1000, toNumber(value, multiplier)]);
  }
}

function compareLabel(a, b) {
  if (a.label < b.label) {
    return -1;
  }
  if (a.label > b.label) {
    return 1;
  }
  return 0;
}

function compareItems(a: ItemQueryData, b: ItemQueryData) {
  const aHaveData = a.lastclock !== null;
  const bHaveData = b.lastclock !== null;

  if (aHaveData > bHaveData) {
    return -1;
  }
  if (aHaveData < bHaveData) {
    return 1;
  }

  if (a.label < b.label) {
    return -1;
  }
  if (a.label > b.label) {
    return 1;
  }

  return 0;
}

function toNumber(apiValue: TimeSeriesValue | boolean, multiplier: number) {
  if (typeof apiValue === 'boolean') {
    return apiValue ? 1 : 0;
  }
  if (typeof apiValue === 'number') {
    return apiValue * multiplier;
  }
  return apiValue;
}

function toRegexp(query: string) {
  return new RegExp(query, 'i');
}
