/**
 *
 * @param unitFromDS unit from ModioAPI
 * @param model the collector-name e.g. suns, modbus, etc
 * @param version version from data aquisition model
 * @returns grafana-converted unit or undefined (for easy override in panel)
 * @link https://github.com/grafana/grafana/blob/master/packages/grafana-data/src/valueFormats/categories.ts
 */
export function grafanaUnit(
  unitFromDS: any,
  model: string,
  version: string
): [string | undefined] | [string | undefined, number] {
  // See https://grafana.com/docs/grafana/latest/panels/field-options/standard-field-options/#custom-units
  let unit = unitFromDS ? `si:${unitFromDS}` : undefined;
  const modelSpecific = {
    suns: {
      C: ['celsius'], // not to confuse with SI-unit Coulomb
    },
    bacnet: {
      // bacnet implements SenML, and should maybe be in defaultUnitMap
      '/': ['percentunit'],
      Cel: ['celsius'],
      m3: ['m3'],
      'm3/s': ['flowlpm', 60 * 1000],
    },
  };
  if (modelSpecific[model] && modelSpecific[model][unitFromDS]) {
    return modelSpecific[model][unitFromDS];
  }
  // keys in this list are currently extracted from modbus_lookup
  const defaultUnitMap = {
    '%': ['percent'],
    'l/h': ['litreh'],
    'l/m': ['flowlpm'],
    'm^3': ['m3'],
    // %RH': ['humidity' woudle give 'Humidity (%H)']?
    A: ['amp'],
    bar: ['pressurebar'],
    // baud':
    days: ['d'],
    dB: ['dB'],
    deg: ['degree'],
    degC: ['celsius'],
    h: ['h'],
    Hz: ['hertz'],
    K: ['kelvin'],
    // kVAh: ['kvoltamp'],
    // kvarh: ['kvoltampreact'],
    kW: ['watt', 1000],
    J: ['watth', 1 / 3600], // J = watt-second
    kWh: ['watth', 1000],
    MWh: ['watth', 1000 * 1000],
    l: ['litre'],
    'l/s': ['flowlpm', 1 / 1000],
    'm^3/h': ['flowcms', 1 / 36000],
    minutes: ['m'],
    // Months':
    Pa: ['pressurepa'],
    ppm: ['ppm'],
    // pulse/kWh':
    rpm: ['rotrpm'],
    s: ['s'],
    V: ['volt'],
    VA: ['voltamp'],
    var: ['voltampreact'],
    W: ['watt'],
    Wh: ['watth'],
  };
  if (defaultUnitMap[unitFromDS]) {
    return defaultUnitMap[unitFromDS];
  }
  return [unit];
}
