import { DataQueryRequest, DataSourceInstanceSettings, DataSourceJsonData, dateTime, stringToMs } from '@grafana/data';
import { ModioApiDataSource } from './DataSource';
import { of } from 'rxjs';
import { ModioApiQuery } from './types';
import * as d3 from 'd3';

const fetchMock = jest.fn();

jest.mock('@grafana/runtime', () => ({
  // @ts-ignore
  ...jest.requireActual('@grafana/runtime'),
  getBackendSrv: () => ({
    fetch: fetchMock,
  }),
  getTemplateSrv: () => ({
    getAdhocFilters: jest.fn(() => [] as any[]),
    replace: jest.fn((a: string, ...rest: any) => a),
  }),
}));

describe('Datasource', () => {
  let ds: ModioApiDataSource;
  const instanceSettings = { url: 'httpz://mock' } as DataSourceInstanceSettings<DataSourceJsonData>;

  beforeEach(() => {
    ds = new ModioApiDataSource(instanceSettings);
    fetchMock.mockReset();
    fetchMock.mockReturnValueOnce(of(createDefaultProtototypeResponse()));
  });

  it('should request Prototypes for siteQuery', async () => {
    const res = await ds.siteQuery('Tha site');
    expect(res).toHaveLength(1);
  });

  it('should get all sites for Customer', async () => {
    const res = await ds.siteQuery('^Customer');
    expect(res).toHaveLength(2);
  });

  it('should get all sites', async () => {
    const res = await ds.siteQuery('.*');
    expect(res).toHaveLength(3);
  });

  it('should get dataframe', async () => {
    const from = dateTime('2020-01-01T12:37:00.000Z');
    const to = dateTime('2020-01-01T13:57:00.000Z');
    const interval = '15m';
    const options: DataQueryRequest<ModioApiQuery> = {
      requestId: 'mock-uuid-for-request',
      interval,
      intervalMs: stringToMs(interval),
      scopedVars: {},
      timezone: 'Europe/Oslo',
      app: 'MockAPP',
      startTime: 0, // not in use?
      range: {
        from,
        to,
        raw: { from: 'starten', to: 'sluttet' },
      },
      targets: [
        {
          refId: 'Q',
          prototypeId: 'uiid-one',
          key: 'modbuzz.mockUnit.keyparts.number.one',
        },
      ],
    };
    const intervalSeconds = stringToMs(interval) / 1000;

    fetchMock.mockReturnValueOnce(
      of({
        status: 200,
        data: [
          {
            configurable: false,
            key: 'modbuzz.mockUnit.keyparts.number.one',
            lastclock: 1624285523,
            lastvalue: 0,
            name: 'A mock name of the sensor with an id of one',
            unit: '',
          },
        ],
      })
    );
    // hist_prepare response
    fetchMock.mockReturnValueOnce(
      of({
        status: 200,
        data: [
          '/v0/aabbccddeeff/modbuzz.mockUnit.keyparts.number.one/history/1577881800/1577885400',
          '/v0/aabbccddeeff/modbuzz.mockUnit.keyparts.number.one/history/1577882700/1577886300',
        ],
      })
    );
    // First history part
    fetchMock.mockReturnValueOnce(
      of({
        status: 200,
        data: [
          [from.unix(), -666], // the first one will be dropped see TimeRange test below
          [from.unix() + intervalSeconds / 2 + 1, 333],
        ],
      })
    );
    // second history_part
    fetchMock.mockReturnValueOnce(
      of({
        status: 200,
        data: [
          [to.unix() - 2 * intervalSeconds - 2, 776],
          [to.unix() - 2 * intervalSeconds - 1, 778],
          [to.unix() - intervalSeconds - 10, 888],
          [to.unix() + 1, 10000],
        ],
      })
    );

    const res = await ds.query(options);

    expect(fetchMock.mock.calls).toMatchInlineSnapshot(`
      Array [
        Array [
          Object {
            "url": "httpz://mock/v0/prototypes",
          },
        ],
        Array [
          Object {
            "url": "httpz://mock/v0/aabbccddeeff",
          },
        ],
        Array [
          Object {
            "url": "httpz://mock/v0/aabbccddeeff/modbuzz.mockUnit.keyparts.number.one/history_prepare/1577882220/1577887020",
          },
        ],
        Array [
          Object {
            "url": "httpz://mock/v0/aabbccddeeff/modbuzz.mockUnit.keyparts.number.one/history/1577881800/1577885400",
          },
        ],
        Array [
          Object {
            "url": "httpz://mock/v0/aabbccddeeff/modbuzz.mockUnit.keyparts.number.one/history/1577882700/1577886300",
          },
        ],
      ]
    `);

    expect(res).toMatchInlineSnapshot(`
      Object {
        "data": Array [
          Object {
            "fields": Array [
              Object {
                "config": Object {
                  "custom": Object {
                    "width": 147,
                  },
                  "displayName": "Time",
                },
                "labels": undefined,
                "name": "time",
                "type": "time",
                "values": Array [
                  1577882700000,
                  1577883600000,
                  1577884500000,
                  1577885400000,
                  1577886300000,
                ],
              },
              Object {
                "config": Object {
                  "custom": Object {},
                  "displayName": undefined,
                  "displayNameFromDS": "A mock name of the sensor with an id of one",
                  "unit": undefined,
                  "writeable": true,
                },
                "labels": Object {
                  "api_key": "Customer_Tha_site_with_a_name",
                  "host": "aabbccddeeff",
                  "itemKey": "modbuzz.mockUnit.keyparts.number.one",
                  "model": "modbuzz",
                  "name": "A mock name of the sensor with an id of one",
                  "prototypeName": "prototypeOne",
                  "unitId": "mockUnit",
                  "version": "keyparts",
                },
                "name": "aabbccddeeff:modbuzz.mockUnit.keyparts.number.one",
                "type": "number",
                "values": Array [
                  333,
                  null,
                  null,
                  777,
                  888,
                ],
              },
            ],
            "meta": undefined,
            "name": undefined,
            "refId": "Q",
          },
        ],
      }
    `);
  });
});

export function createDefaultProtototypeResponse() {
  return {
    status: 200,
    data: [
      {
        id: 'uiid-one',
        site: 'Tha site with a name',
        name: 'prototypeOne',
        host: 'aabbccddeeff',
        api_key: 'Customer_Tha_site_with_a_name',
      },
      {
        id: 'uiid-two',
        site: 'Other site with a name',
        name: 'prototypeTwo',
        host: 'ffffffffffff',
        api_key: 'Customer_Other_site_with_a_name',
      },
      {
        id: 'uiid-three',
        site: 'Separete site and customer',
        name: 'prototypeThree',
        host: '00000000000',
        api_key: 'Foo_Bar_api_key',
      },
    ],
  };
}

/**
 * These are thoughts and discussions on the problem with times, timeranges and rounding.
 *
 * 1. Grafana will complain if query returns points outside requested interval
 * 2. d3.range will return points on or after first argument
 * 3. d3.round then rounds to point before requested interval (ignored for now)
 */

describe('TimeRange', () => {
  it('should create timerange', () => {
    const interval = '15m';
    const myInterval = d3.timeMillisecond.every(stringToMs(interval));

    const from = dateTime('2020-01-01T12:37:00.000Z');
    const to = dateTime('2020-01-01T13:57:00.000Z');
    const range = myInterval!.range(from.toDate(), to.toDate());

    // d3.range: the first date is **after** from-time
    expect(range).toMatchInlineSnapshot(`
      Array [
        2020-01-01T12:45:00.000Z,
        2020-01-01T13:00:00.000Z,
        2020-01-01T13:15:00.000Z,
        2020-01-01T13:30:00.000Z,
        2020-01-01T13:45:00.000Z,
      ]
    `);
    // since we use `round` some initial datapoints will be ignored
    expect(myInterval!.round(from.toDate())).toEqual(new Date('2020-01-01T12:30:00.000Z'));
  });
});
