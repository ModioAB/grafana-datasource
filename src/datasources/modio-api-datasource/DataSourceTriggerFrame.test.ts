import { DataSourceInstanceSettings, DataSourceJsonData, MutableDataFrame } from '@grafana/data';
import { ModioApiDataSource } from './DataSource';
import { of } from 'rxjs';

// @ts-ignore
const fetchMock = jest.fn().mockReturnValueOnce(of(createDefaultProtototypeResponse()));

jest.mock('@grafana/runtime', () => ({
  // @ts-ignore
  ...jest.requireActual('@grafana/runtime'),
  getBackendSrv: () => ({
    fetch: fetchMock,
  }),
  getTemplateSrv: () => ({
    getAdhocFilters: jest.fn(() => [] as any[]),
    replace: jest.fn((a: string, ...rest: any) => a),
  }),
}));

describe('Datasource trigger Frame', () => {
  let ds: ModioApiDataSource;
  const frame = new MutableDataFrame();
  const instanceSettings = {} as DataSourceInstanceSettings<DataSourceJsonData>;

  beforeEach(() => {
    ds = new ModioApiDataSource(instanceSettings);
  });

  it('should mutate triggerFrame by adding fields', async () => {
    Date.now = jest.fn(() => 1617000000 * 1010);
    fetchMock.mockReturnValueOnce(of(createTriggerResponse()));
    const actualFrame = await ds.addTriggersToFrame(frame, 'prototypeOne');
    expect(actualFrame.fields).toHaveLength(7);
    expect(actualFrame).toMatchSnapshot();
  });
});

function createDefaultProtototypeResponse() {
  return {
    status: 200,
    data: [
      {
        id: 'uiid-one',
        site: 'Tha site with a name',
        name: 'prototypeOne',
        host: 'aabbccddeeff',
        api_key: 'Customer_Tha_site_with_a_name',
      },
      {
        id: 'uiid-two',
        site: 'Other site with a name',
        name: 'prototypeTwo',
        host: 'ffffffffffff',
        api_key: 'Customer_Other_site_with_a_name',
      },
      {
        id: 'uiid-three',
        site: 'Separete site and customer',
        name: 'prototypeThree',
        host: '00000000000',
        api_key: 'Foo_Bar_api_key',
      },
    ],
  };
}

function createTriggerResponse() {
  return {
    status: 200,
    data: [
      {
        triggerid: 12345,
        description: 'Read data from foo.bazar',
        hosts: ['aabbccddeeff'],
        value: 'OK',
        lastchange: 1617000000,
        severity: [3, 'C-Larm'],
      },
    ],
  };
}
