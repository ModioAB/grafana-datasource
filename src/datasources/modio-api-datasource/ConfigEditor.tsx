import React, { PureComponent } from 'react';
import { DataSourceHttpSettings } from '@grafana/ui';

import { DataSourceJsonData, DataSourcePluginOptionsEditorProps } from '@grafana/data';

interface Props extends DataSourcePluginOptionsEditorProps<DataSourceJsonData> {}

interface State {}

export class ConfigEditor extends PureComponent<Props, State> {
  render() {
    const { options, onOptionsChange } = this.props;
    const { jsonData } = options;
    const useOAuthPassThru = true;
    setDefault(jsonData, 'tlsAuthWithCACert', true);
    setDefault(jsonData, 'tlsAuth', !useOAuthPassThru);
    setDefault(jsonData, 'oauthPassThru', useOAuthPassThru);
    return (
      <DataSourceHttpSettings
        defaultUrl="https://api.x.modio.se"
        dataSourceConfig={options}
        onChange={onOptionsChange}
      />
    );
  }
}

function setDefault(obj: any, prop: string, value: any) {
  if (obj[prop] === undefined) {
    obj[prop] = value;
  }
  return obj[prop];
}
