import { grafanaUnit } from './grafanaUnit';

describe('Unit conversions', () => {
  it('ModioAPi degC should be celsius', () => {
    expect(grafanaUnit('degC', 'suns', '')).toStrictEqual(['celsius']);
  });

  it('Unknown unit should use si version', () => {
    expect(grafanaUnit('kFoo/Bar', 'any', '')).toStrictEqual(['si:kFoo/Bar']);
  });

  it('C should convert to si unit C normally', () => {
    expect(grafanaUnit('C', 'normal', '')).toStrictEqual(['si:C']);
  });

  it('Suns should convert C to celsius', () => {
    expect(grafanaUnit('C', 'suns', 'any-model')).toStrictEqual(['celsius']);
  });

  it('should convert kWh to Wh with factor 1000', () => {
    expect(grafanaUnit('kWh', 'normal', '')).toStrictEqual(['watth', 1000]);
  });

  it('should convert bacnet 0.0003 m3/s to 18 litre/minute', () => {
    const [unitId, multiplier] = grafanaUnit('m3/s', 'bacnet', 'any');
    expect(unitId).toStrictEqual('flowlpm');
    expect(multiplier! * 0.0003).toStrictEqual(18);
  });
});
