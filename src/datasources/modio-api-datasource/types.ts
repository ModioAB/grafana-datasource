import { DataQuery } from '@grafana/data';

export declare type Aggregation = 'avg' | 'delta';
export interface ModioApiQuery extends DataQuery {
  prototypeId?: string;
  item?: string;
  alias?: string;
  target?: string;
  aggregation?: Aggregation;
}

export interface ApiPrototype {
  api_key: string;
  host: string;
  id: string;
  name: string;
  site: string;
}
