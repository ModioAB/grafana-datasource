import React from 'react';
import { QueryEditor, Props } from './QueryEditor';
import { getByText, render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';

const defaultQuery = {
  alias: 'the alias',
  key: 'device.keypart.more.key.parts',
  prototypeId: 'prototype-uuid-number-one',
  target: 'boxidboxid:device.keypart.more.key.parts',
  refId: 'A',
};
const prototypeOne = {
  id: defaultQuery.prototypeId,
  site: 'Site One',
  name: 'Proto Name One',
  description: 'boxidboxid',
  api_key: 'API_KEY',
  label: 'Site One / Proto Name One',
  value: defaultQuery.prototypeId,
};
const mockOnChange = jest.fn();
const props = {
  onRunQuery: jest.fn(),
  query: defaultQuery,
  onChange: mockOnChange,
  datasource: {
    prototypeQuery: () => Promise.resolve([prototypeOne]),
    itemQuery: () => Promise.resolve([]),
    getPrototype: () => Promise.resolve(prototypeOne),
  } as any,
};

const setup = (testProps?: Partial<Props>) => {
  const editorProps = { ...props, ...testProps };
  return render(<QueryEditor {...editorProps} />);
};

describe('Query editor', () => {
  it('display device with description from prototypeId and key', async () => {
    setup();
    const deviceElement = await screen.findByText(/Device/, { selector: 'label > div' });
    expect(deviceElement).toBeInTheDocument();
    expect(deviceElement.parentElement!.getElementsByTagName('span')[0]).toHaveTextContent('prototype-uuid-number-one');
    const deviceFormSection = deviceElement.parentElement!.parentElement!.parentElement;
    expect(getByText(deviceFormSection!, prototypeOne.label));
  });
});
