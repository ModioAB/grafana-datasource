import React, { ChangeEvent, PureComponent } from 'react';
import { AsyncSelect, Field, Input, HorizontalGroup, RadioButtonGroup } from '@grafana/ui';
import { QueryEditorProps, SelectableValue, dateTimeFormatTimeAgo } from '@grafana/data';

import { ModioApiDataSource } from './DataSource';
import { ModioApiQuery, Aggregation } from './types';

interface QueryState {
  device?: SelectableValue<string>;
  key?: SelectableValue<string>;
  alias?: string;
  target?: string;
  aggregation: Aggregation;
}
export type Props = QueryEditorProps<ModioApiDataSource, ModioApiQuery>;
export class QueryEditor extends PureComponent<Props, QueryState> {
  constructor(props: Props) {
    super(props);
    const { alias, prototypeId, key, aggregation = 'avg' } = props.query;
    this.state = {
      alias,
      device: { value: prototypeId, label: prototypeId },
      key: { value: key, label: key },
      aggregation,
    };
  }

  async componentDidMount() {
    const { prototypeId, key } = this.props.query;
    if (prototypeId) {
      let devices = await this.props.datasource.prototypeQuery(prototypeId);
      if (devices && devices.length === 1) {
        let device = devices[0];
        this.setState({ device });

        if (key) {
          const items = await this.props.datasource.itemQuery(device.value, key);
          if (items && items.length === 1) {
            let item = items[0];
            // restore these non-interpolated values
            item.description = key;
            item.value = key;
            this.setState({ key: item });
          }
        }
      }
    }
  }
  onPrototypeChange = (device: SelectableValue<string>) => {
    const { onChange, query, onRunQuery } = this.props;
    this.setState({ device });
    onChange({ ...query, prototypeId: device.value });
    onRunQuery();
  };

  onItemChange = (key: SelectableValue<string>) => {
    const { onChange, query, onRunQuery } = this.props;
    this.setState({ key });
    onChange({ ...query, key: key.value });
    onRunQuery();
  };
  onAliasChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onChange, query, onRunQuery } = this.props;
    onChange({ ...query, alias: event.target.value });
    onRunQuery();
  };
  onAggChange = (aggregation: Aggregation = 'avg') => {
    const { onChange, query, onRunQuery } = this.props;
    this.setState({ aggregation });
    onChange({ ...query, aggregation });
    onRunQuery();
  };

  render() {
    const { query, datasource } = this.props;
    const fieldWidth = 50;

    return (
      <>
        <HorizontalGroup>
          <Field label="Device" description={this.state.device?.value}>
            <AsyncSelect
              allowCustomValue={true}
              cacheOptions={true}
              defaultOptions={true}
              loadOptions={(query) => datasource.prototypeQuery(query)}
              onChange={this.onPrototypeChange}
              value={this.state.device}
              width={fieldWidth}
            />
          </Field>
          <Field label="Item" description={this.state.key?.value}>
            <AsyncSelect
              allowCustomValue={true}
              cacheOptions={false}
              defaultOptions={true}
              isMulti={false}
              loadOptions={(query) => datasource.itemQuery(this.state.device?.value, query)}
              onChange={this.onItemChange}
              showAllSelectedWhenOpen={true}
              value={this.state.key}
              width={1.5 * fieldWidth}
            />
          </Field>
        </HorizontalGroup>
        <HorizontalGroup>
          <Field label="Alias">
            <Input
              css=""
              name="alias"
              onChange={this.onAliasChange}
              placeholder={this.state.key?.label}
              value={query.alias}
              width={fieldWidth}
            />
          </Field>
          <HorizontalGroup>
            <Field label="Time interval calcs">
              <RadioButtonGroup
                onChange={this.onAggChange}
                options={[
                  { value: 'avg', label: 'Average' },
                  { value: 'delta', label: 'Delta' },
                ]}
                value={this.state.aggregation}
              />
            </Field>
            <Field label="Last timestamp">
              <Input
                css=""
                name="lastclock"
                readOnly
                value={dateTimeFormatTimeAgo(this.state.key?.lastclock * 1000)}
                width={20}
              />
            </Field>
            <Field label="Last value">
              <Input css="" name="lastvalue" readOnly value={this.state.key?.lastvalue} width={10} />
            </Field>
            <Field label="unit">
              <Input css="" name="unit" readOnly value={this.state.key?.unit} width={10} />
            </Field>
          </HorizontalGroup>
        </HorizontalGroup>
      </>
    );
  }
}
