export interface ModioAppSettings {
  customText?: string;
  customCheckbox?: boolean;
}
