import { ComponentClass } from 'react';
import { AppPlugin, AppRootProps } from '@grafana/data';
import { RootPage } from './RootPage';
import { ModioAppSettings } from './types';

export const plugin = new AppPlugin<ModioAppSettings>().setRootPage(
  (RootPage as unknown) as ComponentClass<AppRootProps>
);
// This is how a configuration page is added to `plugin`
// .addConfigPage({
//   title: 'Page 1',
//   icon: 'info-circle',
//   body: ExamplePage1,
//   id: 'page1',
// });
