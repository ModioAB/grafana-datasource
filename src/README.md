# Modio API Datasource

A Grafana Data Source Plugin for Modio API

## Configuration

Configure a new datasource and add targets.

### Timestamps are aligned

Minimal interval is 90s, but can be overriden for each panel.
The average value of datapoints in each interval is used.

## Simple usage

Add targets. For each target select `Prototype` and `Item`.
Optionally also use `Alias` for display name.

## Advanced usage

Add target with simple query.
Use [Regular Expression](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions) in `Prototype` and/or `Item` fields. Make sure to select the last option in list `Create: ...`.

### Delta calculations

`Time interval calcs` defaults to 'Average'. The 'Delta' options is intended for integrating meters such as Energy meters.

### Display name from labels

See background [Grafana Standard field options](https://grafana.com/docs/grafana/latest/panels/field-options/standard-field-options/) .

Currently available labels are:

- api_key
- host
- model
- name
- prototypeName
- unitId
- version

Use field `Display Name` to customize this.

Example: `${__field.labels.unitId}`

Tip: use `${__field.labels}` to see a json-representation of all labels with values.

### Variables

Add a variable of type `Query`.

Currentlty supported is the form

`site:prototype:item`

where each part is optional, also using regular expressions.

| Example                       | Query (case insensitive) |
| ----------------------------- | ------------------------ |
| All Sites                     | `.*`                     |
| All Sites for customer 'Corp' | `Corp`                   |
| All prototypes for Corp       | `corp:.*`                |
| All items with name 'Watts'   | `.*:.*:watts`            |

### Problems (Alerts table)

Use the magic expression `trigger` as item, and use a table as visualisation.

# Development

## Getting started

1. Install dependencies

```BASH
yarn install
```

2. Build plugin in development mode or run in watch mode

```BASH
yarn dev
```

or

```BASH
yarn watch
```

3. Build plugin in production mode

```BASH
yarn build
```

## Setup of visualisation dashboard
To make work properly, two steps need to be taken:

1. Define variables for the dashboard
2. Define panels

### Define variables
The following variables have to be defined for the dashboard:

| Variable name | Definition          |
|---------------|---------------------|
| site          | `.*`                |
| device        | `$site:.*`          |
| item          | `$site:$device:.*`  |

### Define panels

Two different panels need to be created:

1. One panel to dislay trend graphs (visualisation type: `Graph`)
2. One panel used to display the visualisation chart (visualisation type: `Visualisation editor`)

### Graph panel
The graph panel has to be configured with variables defined above:

| Field           | Variable value |
|-----------------|----------------|
| Device          | `$device`      |
| Item            | `$item`        |

In order to not fall victim to Garafana's lazy loading, this graph panel should be the first panel in the dashboard.

NB: once set-up correctly, the trend-graph panel will be hidden. Changes to the panel settings can be made, after clicking on any of the `View Trend`-buttons in the visualisation. In the pull-down on the panels item, select `Edit` and use e.g. Chrome developer tools to decrease width and height of the fullscreen panel.

### Visualisation panel
In the panel options of the visualisation panel, the DOM-id for the graph-panel has to be set explicitly. To the best of my knowledge, this is the only reliable way that still enables us to use any other panels (e.g. for displaying warnings).