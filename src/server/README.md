# This server-directory contains code to for cli applications (nodejs)

It is not part or client code, as long as the entrypoints module.ts (also in subdir) does not implicility include them.

## Reporter

See env-variables at the top of function `getConfig()` in [reporter.ts](reporter.ts) .

Reporter.ts acts on dashboard tags (currently only 'skanska'). All panels of matching dashboards are sent to
the the panel-links that has protocol 'mailto:' and looks like a mailadress.

It has to be configured with a cert to access the data directly from api. 

The dashboard should be saved with

* Dashboard time-interval (like 'Yesterday')
* Dashboard with panels
* panels should have one or more Queries (targets)
* panel should have panel-link(s). One or more email-addresses with protocol 'mailto:'
* panels can override 'Query options' like 'Min interval'

The visualisation options (to the right) has no effect.

### Example invocation

```sh
DRYRUN=false                                         \
GF_SECURITY_ADMIN_PASSWORD=grafana-admin-password    \
GRAFANA_SERVER=https://viz.moodio.se                 \
MAILGUN_KEY=mailgun-api-key                          \
MAILGUN_DOMAIN=mail-domain                           \
SECRET_PATH=/data/secrets                            \
API_URL=https://api.moodio.se                        \
npx ts-node --transpile-only src/server/reporter.ts
```

Above will look for api-cert-files:

```sh
/data/secrets/tls.crt
/data/secrets/tls.key
/data/secrets/caramel.crt
```

### Output format: 

Currently, the only implemented output format is 'SKANSKA trendloggning Format3'

See [reporter.test.ts](reporter.test.ts) for details.