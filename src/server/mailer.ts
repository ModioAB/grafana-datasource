import Mailgun from 'mailgun-js';

export type MailParams = {
  apiKey: string;
  domain?: string;
  mailTo: string;
  data: string[];
  dryRun?: boolean;
};
export async function mailData({
  apiKey,
  domain = 'sandbox0489064b665946d5845bb9d84a6c02fd.mailgun.org',
  mailTo: to,
  data,
  dryRun = false,
}: MailParams) {
  const mg = Mailgun({ apiKey, domain });
  const attachments: Mailgun.Attachment[] = [];
  for (const attachmentText of data) {
    const data = Buffer.from(attachmentText, 'utf8');

    const filename = `data-${attachments.length}.txt`;
    attachments.push(new mg.Attachment({ data, filename }));
  }
  const mailData = {
    from: 'Modio AB <support@modio.se>',
    to,
    text: 'Bifogar trenddata',
    subject: 'Trenddata',
    attachment: attachments,
  };
  if (dryRun) {
    console.log(
      'dryRun',
      mailData,
      attachments
        .map(({ data, filename }) => {
          return [filename, data.toString('utf-8')].join(':\n');
        })
        .join('\n***\n')
    );
  } else {
    await mg.messages().send(mailData);
  }
}
