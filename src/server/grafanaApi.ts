import { URL, URLSearchParams } from 'url';
import fetch, { Headers } from 'node-fetch';

/**
 * A class to interact with a running
 * Grafan instanse via https://grafana.com/docs/grafana/latest/http_api/
 */
export class GrafanaApi {
  headers: Headers;
  baseUrl: string;

  constructor(username = 'admin', password = 'password', baseUrl = 'http://localhost:3000') {
    this.headers = new Headers({
      Authorization: 'Basic ' + Buffer.from(username + ':' + password).toString('base64'),
      'Content-Type': 'application/json',
    });
    this.baseUrl = baseUrl;
  }
  async getDashboard(uuid: string) {
    const url = new URL(`/api/dashboards/uid/${uuid}`, this.baseUrl);
    const resp = await fetch(url, {
      method: 'GET',
      headers: this.headers,
    });
    return await resp.json();
  }
  async searchDashboardsByTag(tag: string) {
    const url = new URL(`/api/search`, this.baseUrl);
    url.search = new URLSearchParams({ tag }).toString();
    const resp = await fetch(url, {
      method: 'GET',
      headers: this.headers,
    });
    return await resp.json();
  }
}
