import './globals-setup'; // this import stays on top.

import { DataFrame, DataQueryRequest, dateMath } from '@grafana/data';
import { ModioApiQuery } from 'datasources/modio-api-datasource/types';
import { buildQuery, convertToSkanskaCsv, getDataSource } from './modio-api-datasource';
import { mailData } from './mailer';
import { GrafanaApi } from './grafanaApi';
import { ApiDataConfig, BuildQuery, MailConfig } from './serverTypings';
import { createMailToListFromPanelLinks } from './reporterHelpers';

/**
 * @returns configuration
 */
async function getConfig() {
  // These are the used env-variables:
  let {
    DRYRUN,
    GF_SECURITY_ADMIN_PASSWORD,
    GRAFANA_SERVER,
    MAILGUN_DOMAIN,
    MAILGUN_KEY,
    SECRET_PATH,
    API_URL,
  } = process.env;

  const apiDataCfg: ApiDataConfig = {
    secretsPath: SECRET_PATH || '/Users/aj/code/modio/certs',
    url: API_URL || 'https://api.molom.modio.se',
  };
  const password = GF_SECURITY_ADMIN_PASSWORD || 'password';
  const baseUrl = GRAFANA_SERVER || 'http://localhost:3000';
  const grafanaApi = new GrafanaApi('admin', password, baseUrl);

  let dataQueryCfgs: BuildQuery[] = [];
  const dashBoardResults = await grafanaApi.searchDashboardsByTag('skanska');
  for (const { uid } of dashBoardResults) {
    const { dashboard } = await grafanaApi.getDashboard(uid);
    const {
      panels,
      timezone = 'Europe/Stockholm',
      time: { from, to },
    } = dashboard;
    for (const { interval = '1h', targets, links, title } of panels) {
      const mailTo = createMailToListFromPanelLinks(links);

      // dateMath.parse below parses dashboard.json `time` value the same way grafana does
      // before passed to datasoruce
      // i.g. 'yesterday' could be written
      // `{from: 'now-1d/d', to: 'now-1d/d'}`
      dataQueryCfgs.push({
        title,
        timezone,
        interval,
        targets,
        from: dateMath.parse(from, false, timezone)!,
        to: dateMath.parse(to, true, timezone)!,
        mailTo,
      });
    }
  }

  const mailCfg: MailConfig = {
    apiKey: MAILGUN_KEY || '',
    domain: MAILGUN_DOMAIN,
    dryRun: (DRYRUN || 'false') === 'true',
  };

  return { mailCfg, apiDataCfg, dataQueryCfgs };
}

async function main() {
  const { mailCfg, apiDataCfg, dataQueryCfgs } = await getConfig();
  const ds = getDataSource(apiDataCfg);
  await ds.testDatasource();
  for (const { interval, timezone, from, to, targets, title, mailTo } of dataQueryCfgs) {
    const queryOptions = buildQuery({ interval, timezone, from, to, targets, mailTo });
    const response = await ds.query(queryOptions as DataQueryRequest<ModioApiQuery>);
    const frames = response.data as DataFrame[];
    const data = frames.map((frame) => convertToSkanskaCsv(frame, from, to, title!));
    await mailData({ ...mailCfg, mailTo, data });
  }
  return 0;
}

main();
