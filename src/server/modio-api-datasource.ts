import {
  DataFrame,
  DataQueryRequest,
  DataSourceInstanceSettings,
  DataSourceJsonData,
  dateTime,
  DateTime,
  getValueFormat,
  ScopedVars,
  stringToMs,
} from '@grafana/data';
import { BackendSrv, FetchResponse, TemplateSrv } from '@grafana/runtime';
import { ModioApiQuery } from 'datasources/modio-api-datasource/types';
import { readFileSync } from 'fs';
import { Agent } from 'https';
import fetch, { Response } from 'node-fetch';
import { from as fromPromise } from 'rxjs';
import { ModioApiDataSource } from '../datasources/modio-api-datasource/DataSource';
import { BuildQuery } from './serverTypings';

export function getDataSource({ secretsPath, url }) {
  const encoding = 'utf-8';
  const agent = new Agent({
    ca: readFileSync(`${secretsPath}/caramel.crt`, { encoding }),
    cert: readFileSync(`${secretsPath}/tls.crt`, { encoding }),
    key: readFileSync(`${secretsPath}/tls.key`, { encoding }),
    keepAlive: true,
  });

  const ds = new ModioApiDataSource({ url } as DataSourceInstanceSettings<DataSourceJsonData>);

  function checkStatus(res) {
    if (res.ok) {
      // res.status >= 200 && res.status < 300
      return res;
    } else {
      res.text().then((text) => console.error(text));
      throw new Error(`${res.statusText} for ${res.url}`);
    }
  }

  const backendSrv: Partial<BackendSrv> = {
    //@ts-expect-error
    fetch: ({ url, headers }) => {
      let resp: Response;
      const apiCall: Promise<Partial<FetchResponse>> = fetch(url, { ...headers, agent })
        .then(checkStatus)
        .then((response) => {
          // duplicate to `resp` visible in the `then` below
          resp = response;
          return response.json();
        })
        .then((data) => {
          return { data, status: resp.status };
        });
      return fromPromise(apiCall);
    },
  };
  ds.backendSrv = backendSrv as BackendSrv;
  const TemplateSrvStub: TemplateSrv = {
    replace: (target?: string, scopedVars?: ScopedVars, format?: string | Function) => {
      return target || '';
    },
    getVariables: () => [],
  };
  ds.templateSrv = TemplateSrvStub;
  return ds;
}
export function buildQuery({ interval, timezone, from, to, targets }: BuildQuery) {
  return {
    interval,
    intervalMs: stringToMs(interval),
    timezone,
    range: { from, to, raw: { from: '', to: '' } }, // empty raws for typings...
    targets,
  } as Partial<DataQueryRequest<ModioApiQuery>>;
}

/**
 * See Format 3 in
 * {@link https://gitlab.com/ModioAB/agile/uploads/2bb7f2c549ddd532b241470c5c591248/Krav_p%C3%A5_trendloggning_och_export_av_trenddata.pdf}
 */

export function convertToSkanskaCsv(frame: DataFrame, from: DateTime, to: DateTime, projectId: string) {
  const SEP = ';';
  const NEWLINE = '\n';
  function dateTimeFromTz(time: DateTime) {
    const dateFmt = 'YYYY-MM-DD';
    const timeFmt = 'HH:mm:ss';
    return [time.format(dateFmt), time.format(timeFmt)];
  }

  // we assign address with any of the items api_key (last wins)
  // currently relying on a cert that only has access to one site (uniq api_key)
  let address = '';
  const [timeField, ...dataFields] = frame.fields;
  const dataHeader = dataFields.map(({ config, labels }) => {
    address = labels!.api_key;
    const { suffix = '' } = getValueFormat(config.unit)(0);
    return `${config.displayNameFromDS}${suffix}`
      .replace(/ /g, '_') // No spaces from 1.4
      .replace(/[^a-zA-Z0-9_%]/g, ''); // my interpretation of allowed chars from 1.3 and footnote iii
  });
  const times = timeField.values;
  // Add some simple header rows in two columns:
  let csv: string[][] = [];
  csv.push(['Adress', address]);
  csv.push(['Rapport', projectId]);
  csv.push(['Starttid', ...dateTimeFromTz(from)]);
  csv.push(['Stopptid', ...dateTimeFromTz(to)]);

  // Here we start the real table data, that contains all columns
  // `dataHeader` is in form ['meter_one_kHz','second_meter_unit',...]
  csv.push(['Datum', 'Tid', ...dataHeader]);

  // For each time we add values for each field e.g.
  // [2020-01-02,01:15,50.1,123,...]
  // [2020-01-02,01:30,51.0,100,...]
  for (let ridx = 0; ridx < times.length; ridx++) {
    const timeMs = times.get(ridx);
    const [date, time] = dateTimeFromTz(dateTime(timeMs));
    const dataRow = [
      date,
      time,
      ...dataFields.map((df) => {
        let val = df.values.get(ridx);
        if (val === undefined || val === null) {
          val = 'NaN'; // Footnote vii in page 2 link above
        }
        return `${val}`.replace('.', ',');
      }),
    ];
    csv.push(dataRow);
  }
  return csv.map((row) => row.join(SEP)).join(NEWLINE);
}
