import { PanelLink } from './serverTypings';

export function createMailToListFromPanelLinks(links: PanelLink[]) {
  let mailTos: string[] = [];
  for (const link of links) {
    try {
      const url = new URL(link.url);
      if (url.protocol === 'mailto:') {
        if (/^\S+@\S+$/.test(url.pathname)) {
          mailTos.push(url.pathname);
        }
      }
    } catch (error) {
      console.warn(`PanelLink ${link}`, error);
    }
  }
  return mailTos.join(',');
}
