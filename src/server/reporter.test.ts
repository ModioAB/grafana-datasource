import { dateTime, MutableDataFrame } from '@grafana/data';
import { buildQuery, getDataSource, convertToSkanskaCsv } from './modio-api-datasource';
import { createMailToListFromPanelLinks } from './reporterHelpers';
import { PanelLink } from './serverTypings';

describe('Datasource', () => {
  it('should getDataSource with fetch', async () => {
    const ds = getDataSource({ secretsPath: `${__dirname}/mocks`, url: 'http://dummy' });
    expect(ds.backendSrv).toHaveProperty('fetch');
  });

  it('should build a query', () => {
    const query = buildQuery({
      interval: '13m',
      timezone: 'Europe/Oslo',
      from: dateTime('2020-01-01T23:00:00Z'),
      to: dateTime('2020-01-03T11:13:14Z'),
      targets: [
        {
          refId: 'Q',
          prototypeId: 'Sajt Gutte',
          key: 'Enerji.*.slutt$',
          format: 'skanska',
        },
      ],
      mailTo: 'haakon@lillehammer.norsk',
    });
    expect(query).toMatchInlineSnapshot(`
      Object {
        "interval": "13m",
        "intervalMs": 780000,
        "range": Object {
          "from": "2020-01-01T23:00:00.000Z",
          "raw": Object {
            "from": "",
            "to": "",
          },
          "to": "2020-01-03T11:13:14.000Z",
        },
        "targets": Array [
          Object {
            "format": "skanska",
            "key": "Enerji.*.slutt$",
            "prototypeId": "Sajt Gutte",
            "refId": "Q",
          },
        ],
        "timezone": "Europe/Oslo",
      }
    `);
  });
  it('should convertToSkanskaFormatCsvFormat', () => {
    const labels = { api_key: 'Hagaporten3' };
    const frame = new MutableDataFrame({
      refId: 'A',
      fields: [
        { name: 'time' },
        { name: 'FK13_FO', config: { displayNameFromDS: 'FK13? FO', unit: 'A' }, labels },
        { name: 'FK13_FO', config: { displayNameFromDS: 'FK13_FO', unit: 'KW' }, labels },
        { name: 'FK13_FO', config: { displayNameFromDS: 'FK13_#FO', unit: 'Hz' }, labels },
        { name: 'FK13_FO', config: { displayNameFromDS: 'FK13_FO', unit: '°' }, labels },
      ],
    });
    frame.appendRow(['2008-10-18 00:02:12', 1.43, 0.0862, 20, 37]);
    frame.appendRow(['2008-10-18 00:08:33', 1.43, 0.0855, 20, 37]);
    frame.appendRow(['2008-10-18 00:14:56', 1.43, 0.087, 20, 37]);
    frame.appendRow(['2008-10-18 15:01:59', 1.41, 0.111, null, 37]);
    frame.appendRow(['2008-10-18 16:02:01', 1.41, 0.111, 0, 38]);
    const from = dateTime('2008-10-18 00:00:00');
    const to = dateTime('2008-10-19 00:00:00');
    const projectId = '370103_FK13PK11';
    const csvResult = convertToSkanskaCsv(frame, from, to, projectId);

    // inline snapshot built on example in Format 3 here:
    // https://gitlab.com/ModioAB/agile/uploads/2bb7f2c549ddd532b241470c5c591248/Krav_p%C3%A5_trendloggning_och_export_av_trenddata.pdf
    expect(csvResult).toMatchInlineSnapshot(`
      "Adress;Hagaporten3
      Rapport;370103_FK13PK11
      Starttid;2008-10-18;00:00:00
      Stopptid;2008-10-19;00:00:00
      Datum;Tid;FK13_FO_A;FK13_FO_KW;FK13_FO_Hz;FK13_FO_
      2008-10-18;00:02:12;1,43;0,0862;20;37
      2008-10-18;00:08:33;1,43;0,0855;20;37
      2008-10-18;00:14:56;1,43;0,087;20;37
      2008-10-18;15:01:59;1,41;0,111;NaN;37
      2008-10-18;16:02:01;1,41;0,111;0;38"
    `);
  });
});

describe('helpers', () => {
  it('parses panel links to proper mailgun toAdress list', () => {
    const panelLinks: PanelLink[] = [
      {
        title: 'a nice link title for mail link',
        url: 'not.a.valid.mailto.protocol@mock.no',
      },
      {
        title: 'http link',
        url: 'http://a.link.no',
      },
      {
        title: 'valid email',
        url: 'mailto:a.valid.mail@mock.no',
      },
      {
        title: 'second valid email',
        url: 'mailto:valid@mock.no',
      },
    ];
    const actualMailTo = createMailToListFromPanelLinks(panelLinks);
    expect(actualMailTo).toEqual('a.valid.mail@mock.no,valid@mock.no');
  });
});
