/**
 * In order to execute grafana code in node js
 * this file has to be included before importing
 * grafana-ui things.
 * The needed properties might change in future
 */

import { JSDOM } from 'jsdom';
const neededProperties = ['navigator', 'Element', 'document', 'devicePixelRatio', 'location'];
const { window } = new JSDOM();
globalThis.window = window as any;
for (const prop of neededProperties) {
  globalThis[prop] = window[prop];
}
