import { DateTime, TimeZone } from '@grafana/data';

export interface BuildQuery {
  title?: string;
  interval: string;
  timezone: TimeZone;
  from: DateTime;
  to: DateTime;
  targets: [
    {
      refId: string;
      prototypeId: string;
      key: string;
      format: string;
    }
  ];
  mailTo: string;
}

export interface MailConfig {
  apiKey: string;
  domain: string | undefined;
  dryRun: boolean;
}

export interface ApiDataConfig {
  secretsPath: string;
  url: string;
}

export type PanelLink = {
  url: string;
  title: string;
};
