import { AppRootProps } from '@grafana/data';
import { getDataSourceSrv } from '@grafana/runtime';
import { Card, Icon } from '@grafana/ui';
import { ModioApiDataSource } from 'datasources/modio-api-datasource/DataSource';
import React, { FC, useEffect, useState } from 'react';
import { ApiPrototype } from 'datasources/modio-api-datasource/types';

const dashboardUid = 'MODgH_uMk';

export const SiteList: FC<AppRootProps> = ({ query, path, meta }) => {
  const [sites, setSites] = useState<ApiPrototype[]>([]);
  useEffect(() => {
    if (!sites.length) {
      getSites();
    }
  }, [sites.length]);

  const getSites = async () => {
    const srv = getDataSourceSrv();
    const ds = await srv.getList({ pluginId: 'modioab-api-datasource' });
    const modio = ds.find((d) => d.isDefault);
    if (modio) {
      const api = new ModioApiDataSource(modio);
      if (api) {
        setSites(await api.siteQuery('.*'));
      }
    }
  };
  return (
    <div>
      <ul style={{ listStyle: 'none' }}>
        {sites.length === 0 && <Icon name="fa fa-spinner"></Icon>}
        {sites.map((proto) => (
          <li key={proto.api_key}>
            <Card
              heading={proto.site}
              description={proto.api_key}
              href={`/d/${dashboardUid}/?var-site=${proto.api_key}`}
            />
          </li>
        ))}
      </ul>
    </div>
  );
};
