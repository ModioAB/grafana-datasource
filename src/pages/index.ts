import { AppRootProps } from '@grafana/data';
import { SiteList } from './SiteList';
import { User } from './User';

export type PageDefinition = {
  component: React.FC<AppRootProps>;
  icon: string;
  id: string;
  text: string;
};

export const pages: PageDefinition[] = [
  {
    component: SiteList,
    icon: 'file-alt',
    id: 'sitelist',
    text: 'Sites',
  },
  {
    component: User,
    icon: 'user',
    id: 'profile',
    text: 'Profile',
  },
];
