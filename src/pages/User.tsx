import React, { FC } from 'react';

export const User: FC = () => {
  const { user } = (window as any).grafanaBootData;
  return (
    <div>
      You are logged in as {user?.name} {user?.email}
    </div>
  );
};
