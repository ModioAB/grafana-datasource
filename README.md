Modio API datasource
====================

This is a Grafana plugin for using data from the [Modio
API](https://modio.se/docs/api).

