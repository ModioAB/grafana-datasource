IMAGE_REPO = $(IMAGE_REGISTRY_NAMESPACE)/grafana-datasource/grafana
IMAGE_FILES += dist

CLEANUP_FILES += dist

include registry.mk
include build.mk

# Workaround hack for hardcoded md4 hash call in transitive dependency on
# webpack@4 causing ERR_OSSL_EVP_UNSUPPORTED in tests
#
# https://github.com/nodejs/node/releases/tag/v17.0.0
NODE_OPTIONS += --openssl-legacy-provider
export NODE_OPTIONS

dist:
	yarn install
	yarn dev            # since this also runs typecheck
	yarn build
